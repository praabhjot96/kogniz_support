#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 27 02:42:59 2020

@author: prabhjotsingh
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov  4 20:11:50 2020

@author: prabhjotsingh
"""

import pandas as pd
import numpy as np
import sys
import os
import re
import time
from datetime import datetime, date, time, timedelta
import argparse
import json
import requests
from ftplib import FTP_TLS
import files_sdk
import logging

files_sdk.set_api_key("69f4b6d7e54e66bbcfa6e2627fc4564586a66c97c0012442a513898fcc37cf04")
def getoptions():
    
    global arguments,api_key,args
    list_final=[]
    ap = argparse.ArgumentParser()

    ap.add_argument("-k", "--api_key",dest ='key_file',required=True,help="api key file path")
    ap.add_argument("-f1", "--file_today",dest ='file1',help="file asof today")
    ap.add_argument("-day",dest='run_day',help="Expected values are yest or today")
    ap.add_argument("-date",dest='run_date',help="Expected date in format yyyymmdd")
    args, leftovers = ap.parse_known_args()
    
    arguments = args
    
    if (os.path.isfile(args.key_file) !=True):
        
        log(os.path.isfile(args.key_file),'console')
        usage()
        
    else:
        with open(args.key_file, 'r') as file:
            api_key= file.read().replace('\n', '')
        
        
    if (args.file1 is  None):
          log("Fetching files from Files.com",'console')
          
          files=fetch_file()
          return files
      
    else   : 
        if (os.path.isfile(args.file1) !=True):
            log(os.path.isfile(args.file1),'console')
            usage()
            
        else:
            log("file1:{}".format(args.file1),'console')
            list_final.append(args.file1)
            return list_final


def prepare_dataframe(args):
    
    
    for file1 in args:
        print("preparing dataframe")
        print(file1)
        df1 = pd.read_csv(file1)
        #df1.columns =['Person Num','','Effective date','PunchTime','','Punchtype']
        #print(df1)
        
        l = ["PersonNum","","Effective date","PunchTime","","Punchtype","","","","","","","","",""]
        
        with open(file1, 'r') as data_file:
            lines = data_file.readlines()
            lines[0]= ",".join(l)+"\n" # replace first line, the "header" with list contents
            with open(file1, 'w') as out_data:
                for line in lines: # write updated lines
                    out_data.write(line)
        df1 = pd.read_csv(file1)
        print(df1)
        
        df1.to_excel('timesheet.xlsx',index=False)
        
        
        
        df3 = pd.read_excel('timesheet.xlsx')
        print (df3)
        i= df3[(df3['PersonNum']=='TT')].index
        print(i)
        df4=df3.drop(i)
        print(df4)
        df4['Person Num'] = df4['PersonNum'].astype(str).astype(int)
        df4.dropna(subset=['PersonNum'], inplace=True)
        
        df4_sorted=df4.sort_values(by=['Person Num'], ascending=True)
        
        
        #df3_sorted = df3_sorted.apply(lambda x: x.astype(str).str.lower())
        #print(df4_sorted)
        api_call(df4_sorted)
    
def api_call(emp_df):
    
    new_emp_count=0
    updated_emp_count=0
    id_processed=[]
    emp_df.rename(columns={'Person Num': 'Person_Num'}, inplace=True)
    emp_df_sorted=emp_df.sort_index(axis=0)
    print("sorted")
    print(emp_df_sorted)
    timesheet_body=[]
    
    pin=0
    pout=0
    k=0
    emp_df['Effective date'] = pd.to_datetime(emp_df['Effective date'], format='%Y%m%d')
    post_url = 'https://api.kogniz.com/enrolled-management/people/inoutBatchMulti'
    timesheet_header = {'X-Kogniz-api-key':'%s' %api_key,
                        'accept':'application/json','Content-Type':'application/json',
                        'data':'timesheet_json'}
    for i in range(len(emp_df['Person_Num'])):

        if(emp_df['Person_Num'].iloc[i] not in id_processed):
            #print(id_processed)
            
            id_processed.append(emp_df['Person_Num'].iloc[i])
            emp_id=[int(emp_df['Person_Num'].iloc[i]),]
            temp_df=emp_df[emp_df['Person_Num'].isin(emp_id)]
            sorted_temp_df=temp_df.sort_index(axis=0)
            
    
            print(emp_id)
            print(sorted_temp_df)
            j=0
            print(len(sorted_temp_df))
            while(j<len(sorted_temp_df)):
                print(j)
                if(j+1<len(sorted_temp_df)):
                    if(sorted_temp_df['Punchtype'].iloc[j]=='W' and sorted_temp_df['Punchtype'].iloc[j+1]=='O'):
                        
                        punchin= datetime.strptime(sorted_temp_df['PunchTime'].iloc[j],'%H:%M').time()
                        punchout= datetime.strptime(sorted_temp_df['PunchTime'].iloc[j+1],'%H:%M').time()
                        punchin_temp=str(sorted_temp_df['Effective date'].iloc[j].date())+'T'+str(punchin)+'Z'
            
                        punchout_temp=str(sorted_temp_df['Effective date'].iloc[j+1].date())+'T'+str(punchout)+'Z'
                        empid=str(sorted_temp_df['Person_Num'].iloc[j])
                        employee_time = {
                            "employeeId": empid ,
                            "ts_in": punchin_temp,
                            "ts_out": punchout_temp,
                            "use_localtime": True
                            }
                        k=k+1
                        timesheet_body.append(employee_time)
                        timesheet_json=json.dumps(timesheet_body)
                        my_array = np.asarray(timesheet_body)
                        
                        j=j+2
                        if(k==100):   
                            print(k)
                            print(post_url)
                            print(timesheet_json)
                            print(type(timesheet_json))
                            
                            response = requests.post(post_url,headers=timesheet_header,data=timesheet_json) 
                            k=0
                            timesheet_body.clear()
                
                            if ('error' in str(response.content) or 'error' in response):
                                log(response,'warn')
                                log(response.content,'warn')
                                log("Error occured posting timesheet record for  employee id  %s"%emp_id[0],'console')
                            else:
                                log('successfull','console')
                                log(response,'warn')
                                log(response.content,'warn')
                            
                        
                    else:
                        print("missing entry")
                        print(sorted_temp_df['PunchTime'].iloc[j])
                        print(sorted_temp_df['Punchtype'].iloc[j])
                        j=j+1
                else:
                    print("completed for this id")
                    break
                
"""                 
            if (len(punchin_lst)==len(punchout_lst)):
                #punchin_lst.sort()
                #punchout_lst.sort()
                #date_in.sort()
                #date_out.sort()
                print(punchin_lst)
                print(punchout_lst)
                print(date_in)
                print(date_out)
                for t in range(len(punchin_lst)):
                    
                    punchin_temp=str(date_in[t])+'T'+str(punchin_lst[t])+'Z'
        
                    punchout_temp=str(date_out[t])+'T'+str(punchout_lst[t])+'Z'
                    empid=str(sorted_temp_df['Person_Num'].iloc[j])
                    employee_time = {
                        "employeeId": empid ,
                        "ts_in": punchin_temp,
                        "ts_out": punchout_temp,
                        "use_localtime": True
                        }
                    k=k+1
                    timesheet_body.append(employee_time)
                    timesheet_json=json.dumps(timesheet_body)
                    my_array = np.asarray(timesheet_body)
                    print(timesheet_body)
        
        

                    if(k==100):   
                        print(k)
                        print(post_url)
                        print(timesheet_json)
                        print(type(timesheet_json))
                        
                        response = requests.post(post_url,headers=timesheet_header,data=timesheet_json) 
                        k=0
                        timesheet_body.clear()
            
                        if ('error' in str(response.content) or 'error' in response):
                            log(response,'warn')
                            log(response.content,'warn')
                            log("Error occured posting timesheet record for  employee id  %s"%emp_id[0],'console')
                        else:
                            log('successfull','console')
                            log(response,'warn')
                            log(response.content,'warn')
            
                   
            else:
                
                print('punchin punchout are not sufficient for empid %s' %temp_df['Person_Num'].iloc[j])
                print('punchin length %s' %len(punchin_lst))
                print('punchout length %s' %len(punchout_lst))

                continue
                        
            if not punchin_lst:
                print("No punchin time found for employee id %s" % emp_df['Person_Num'].iloc[i])
                
                continue
            if not punchout_lst:
                print("No punchout time found for employee id %s" % emp_df['Person_Num'].iloc[i])
                
                continue
            """
def fetch_file():

    

    file_list=[]
    path_latest = "tractor/"
    
    if(args.run_date is not None):
        dt=datetime.strptime(args.run_date, '%Y%m%d')
    else:
        if(args.run_day=='today' or args.run_day is None):
            dt = date.today()
        elif(args.run_day=='yest'):
            dt = date.today() - timedelta(1)
        else:
            log('Unexpected  value passed in day option','console')
            exit(1)

    run_date=dt.strftime("%Y%m%d")
    
    
    
    log('fetching files for below dates','console')
    log(run_date,'console')
    
            
    files_sdk.folder.list_for("tractor/")
    for f in files_sdk.folder.list_for(path_latest, {
        "page": 1,
        "sort_by[modified_at_datetime]": "desc"
    }):
        
        timesheet_file = re.search("ReflexisPunchData_%s" %run_date, f.display_name)
        timesheet_ssc_file = re.search("ReflexisPunchData_SSC_DC_GA6_%s" %run_date, f.display_name)
        
        print(timesheet_file)
        if(timesheet_file!=None ):
            log("run_date term file found",'console' )
            log(f.display_name,'console')
            f.download_file(f.display_name)
            
            run_date_timesheet = os.path.join(os.getcwd(), '%s'%f.display_name )
            new_timesheet = os.path.join(os.getcwd(), 'ReflexisPunchData_%s.csv'%run_date)
            os.rename(run_date_timesheet,new_timesheet)
            log(new_timesheet,'console')
            
        elif(timesheet_ssc_file!=None):
            log(f.display_name,'console')
            f.download_file(f.display_name)
            
            run_date_timesheet_ssc = os.path.join(os.getcwd(), '%s'%f.display_name )
            new_timesheet_ssc = os.path.join(os.getcwd(), 'ReflexisPunchData_SSC_%s.csv'%run_date)
            os.rename(run_date_timesheet_ssc,new_timesheet_ssc)
            log(new_timesheet_ssc,'console')
          
            
    if(new_timesheet and new_timesheet_ssc):
        log("Fetched both timesheet  file from Files.com successfully",'console')
        file_list.append(new_timesheet)
        file_list.append(new_timesheet_ssc)
        return file_list
    elif(new_timesheet):
        log("Fetched  timesheet main file from Files.com successfully",'console')
        file_list.append(new_timesheet)
        return file_list
    elif(new_timesheet_ssc):
        log("Fetched both timesheet SSC file from Files.com successfully",'console')
        file_list.append(new_timesheet_ssc)
        return file_list
    else:
        log("No file found for run_date.Please check",'console')
        exit(1)
 
def cleanup_call():
    
    cleanup_url='https://apps.kogniz.com/event-data/proximity/logbook/cleanup-clock-in-out'
    
    cleanup_body={
        "ts_cutoff":"2020-12-16T12:00:00Z",
        "inclusions": [
                    {
                        "n": "Location",
                        "v": "Stores"
                        },
                    {
                        "n": "Compensation Frequency",
                        "v": "H"
                        }
                    ]
                }
    cleanup_body_json=json.dumps(cleanup_body)
    header={'X-Kogniz-api-key':'%s' %api_key,
            'accept':'application/json','Content-Type':'application/json'}
    

        
def log(msg,code):
    
    if (code=='console'):
       print(msg)

    if(code=='warn'):
        logging.warning(msg)
        
def usage( ):
    print ("Error: -file1 or -file2 does not exist or wrng path given.Please check.")
    sys.exit(1)


    
if __name__ == "__main__":  

    files=getoptions()
    prepare_dataframe(files)
      
    
        
        

