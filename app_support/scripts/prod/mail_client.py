import logging
import smtplib
import imaplib
import email
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

def send_mail(subject,msg):
   
    
    
    
    
    #The mail addresses and password
    sender_address = 'prabhjot@kogniz.com'
    sender_pass = 'Kognizsupportdesk_admin@1230'
    receiver_address = 'support@kogniz.com'
    #Setup the MIME
    message = MIMEMultipart()
    message['From'] = sender_address
    message['To'] = receiver_address
    message['Subject'] = subject   #The subject line
    #The body and the attachments for the mail
    message.attach(MIMEText(msg, 'plain'))
    #Create SMTP session for sending the mail
    session = smtplib.SMTP('smtp.gmail.com', 587) #use gmail with port
    session.starttls() #enable security
    session.login(sender_address, sender_pass) #login with mail_id and password
    text = message.as_string()
    session.sendmail(sender_address, receiver_address, text)
    session.quit()
    print('Mail Sent')
    
    
def get_email_ids(mail, label='INBOX', criteria='ALL', max_mails_to_look=20):
    
    
    mail.select(label)
    type, data = mail.search(None, criteria)
    mail_ids = data[0]
    id_list = mail_ids.split()
    # revers so that latest are at front
    id_list.reverse()
    id_list = id_list[: min(len(id_list), max_mails_to_look)]
    
    return id_list

def search_by_subject(email_ids_list, subject_substring,mail):
    for email_id in email_ids_list:
        msg = get_email_msg(email_id,mail)
        if "Subject" in msg.keys():
            subject = msg.get("Subject", "")
            print("{}".format(subject))
            if subject_substring.lower() in subject.lower():
                return msg

    return None

def get_email_msg(email_id,mail):
    email_id = str(int(email_id))
    type, data = mail.fetch(str(email_id), '(RFC822)')
    for response_part in data:
        if isinstance(response_part, tuple):
            return email.message_from_bytes(response_part[1])
        
        
def get_mail_client(email_address):
    SMTP_SERVER = "imap.gmail.com"
    SMTP_PORT = 993

    mail = imaplib.IMAP4_SSL(SMTP_SERVER)
    mail.login(email_address,'Kognizsupportdesk_admin@1230')
    
    id_list=get_email_ids(mail)
    msg=search_by_subject(id_list, "A test mail sent by Python. It has an attachment.",mail)
    print(msg)
    
    
    
    
def log(msg,code):
    
    if (code=='console'):
       print(msg)

    if(code=='warn'):
        logging.warning(msg)
