#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Nov 27 02:42:59 2020

@author: prabhjotsingh
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Nov  4 20:11:50 2020

@author: prabhjotsingh
"""

import pandas as pd
import numpy as np
import sys
import os
import re
import time
from datetime import datetime, date, time, timedelta
import argparse
import json
import requests
from ftplib import FTP_TLS
import files_sdk
import logging
import time
import email_client

files_sdk.set_api_key("69f4b6d7e54e66bbcfa6e2627fc4564586a66c97c0012442a513898fcc37cf04")
today_source_path,today_dest_path,yest_source_path,yest_dest_path='None'
dt_object = datetime.now()

def getoptions():
    
    global arguments,api_key,args
    print("JOB START TIME%s",dt_object)    
    list_final=[]
    ap = argparse.ArgumentParser()

    ap.add_argument("-k", "--api_key",dest ='key_file',required=True,help="api key file path")
    ap.add_argument("-f1", "--file_today",dest ='file1',help="file asof today")
    ap.add_argument("-day",dest='run_day',help="Expected values are yest or today")
    ap.add_argument("-date",dest='run_date',help="Expected date in format yyyymmdd")
    ap.add_argument("-c", "--cleanup_only",help="only cleanup will run")
    ap.add_argument("-flag",help="only cleanup will run")
    args, leftovers = ap.parse_known_args()
    
    arguments = args
    
    if (os.path.isfile(args.key_file) !=True):
    
        log(os.path.isfile(args.key_file),'console')
        usage()
        
    else:
        with open(args.key_file, 'r') as file:
            api_key= file.read().replace('\n', '')
        
    if(args.cleanup_only  is not None):
        cleanup_call()
        log("cleanup process completed",'console')
        return False
    else:
        if (args.file1 is  None):
              log("Fetching files from Files.com",'console')
              
              files=fetch_file()
              return files
          
        else   : 
            if (os.path.isfile(args.file1) !=True):
                log(os.path.isfile(args.file1),'console')
                usage()
                
            else:
                log("file1:{}".format(args.file1),'console')
                list_final.append(args.file1)
                return list_final


def prepare_dataframe(files):
    
    print(files)
    for f in files:
        print("preparing dataframe")
        print("running for file:%s" ,f)
        df1 = pd.read_csv(f)
        
        
        df1.to_excel('timesheet.xlsx',index=False)
        
        
        
        df3 = pd.read_excel('timesheet.xlsx')
        print (df3)

        df3['EmployeeID'] = df3['EmployeeID'].astype(str).astype(int)
        df3.dropna(subset=['EmployeeID'], inplace=True)
        
        df3_sorted=df3.sort_values(by=['EmployeeID'], ascending=True)

        api_call(df3_sorted)
    
def api_call(emp_df):
    
    new_emp_count=0
    updated_emp_count=0
    id_processed=[]
    #emp_df.rename(columns={'EmployeeID': 'EmployeeID'}, inplace=True)
    emp_df_sorted=emp_df.sort_index(axis=0)
    print("sorted")
    print(emp_df_sorted)
    timesheet_body= [None]*50
    timesheet_body_sec=[]
    left_timesheet_body=[]
    
    pin=0
    pout=0
    k=0
    k_sec=0
    processed_count=0
    emp_df['EffectiveDate'] = pd.to_datetime(emp_df['EffectiveDate'], format='%Y%m%d')
    
    timesheet_header = {'X-Kogniz-api-key':'%s' %api_key,
                        'accept':'application/json','Content-Type':'application/json',
                        'data':'timesheet_json'}
    for i in range(len(emp_df['EmployeeID'])):

        if(emp_df['EmployeeID'].iloc[i] not in id_processed):
            #print(id_processed)
            
            id_processed.append(emp_df['EmployeeID'].iloc[i])
            emp_id=[int(emp_df['EmployeeID'].iloc[i]),]
            temp_df=emp_df[emp_df['EmployeeID'].isin(emp_id)]
            sorted_temp_df=temp_df.sort_index(axis=0)
            
    
            #print(emp_id)
            #print(sorted_temp_df)
            j=0
            #print(len(sorted_temp_df))
            processed_count=processed_count + len(sorted_temp_df)
            left_count=len(emp_df['EmployeeID'])-processed_count
            print(sorted_temp_df)
            empid=str(sorted_temp_df['EmployeeID'].iloc[j])
            while(j<len(sorted_temp_df)):
                
                if(j+1<len(sorted_temp_df)):
                    if(sorted_temp_df['PunchType'].iloc[j]=='W' and sorted_temp_df['PunchType'].iloc[j+1]=='O'):
                        print("i am in WO")
                        punchin= datetime.strptime(sorted_temp_df['PunchTime'].iloc[j],'%H:%M').time()
                        punchout= datetime.strptime(sorted_temp_df['PunchTime'].iloc[j+1],'%H:%M').time()
                        punchin_temp=str(sorted_temp_df['EffectiveDate'].iloc[j].date())+'T'+str(punchin)+'Z'
            
                        punchout_temp=str(sorted_temp_df['EffectiveDate'].iloc[j+1].date())+'T'+str(punchout)+'Z'
                        
                        employee_time = {
                            "employeeId": empid ,
                            "ts_in": punchin_temp,
                            "ts_out": punchout_temp,
                            "use_localtime": True
                            }
                        print(employee_time)
                        if(k<len(timesheet_body)):
                           
                            timesheet_body[k]=employee_time
                                
                            timesheet_json=json.dumps(timesheet_body)
                            k=k+1
                        
                        j=j+2
                        
                        
                    elif(sorted_temp_df['PunchType'].iloc[j]=='W' and sorted_temp_df['PunchType'].iloc[j+1]=='W'):
                        print("i am in WW")
                        punchin= datetime.strptime(sorted_temp_df['PunchTime'].iloc[j],'%H:%M').time()
                        punchin_sec= datetime.strptime(sorted_temp_df['PunchTime'].iloc[j+1],'%H:%M').time()
                        
                        punchin_temp=str(sorted_temp_df['EffectiveDate'].iloc[j].date())+'T'+str(punchin)+'Z'
                        punchin_sec_temp=str(sorted_temp_df['EffectiveDate'].iloc[j+1].date())+'T'+str(punchin_sec)+'Z'
                        
                        punchout_temp= str(sorted_temp_df['EffectiveDate'].iloc[j].date()+timedelta(1))+'T'+str(punchin)+'Z'
                        punchout_sec_temp= str(sorted_temp_df['EffectiveDate'].iloc[j+1].date()+timedelta(1))+'T'+str(punchin_sec)+'Z'
                        
                        employee_time = {
                            "employeeId": empid ,
                            "ts_in": punchin_temp,
                            "ts_out": punchout_temp,
                            "use_localtime": True
                            }
                        employee_sec_time={
                            "employeeId": empid ,
                            "ts_in": punchin_sec_temp,
                            "ts_out": punchout_sec_temp,
                            "use_localtime": True
                            }
                        print(employee_time)
               #         print(employee_time_sec)
                        if(k+1<len(timesheet_body)):
                           
                            timesheet_body[k]=employee_time
                            timesheet_body[k+1]=employee_sec_time
                            timesheet_json=json.dumps(timesheet_body)
                            k=k+2
                        """
                        if(k_sec<len(timesheet_body_sec)):
                            timesheet_body_sec[k_sec]=employee_time_sec
                            timesheet_json_sec=json.dumps(timesheet_body_sec)
                            k_sec=k_sec+1
                        """
                        j=j+2
                        
                    elif(sorted_temp_df['PunchType'].iloc[j]=='O' and sorted_temp_df['PunchType'].iloc[j+1]=='W'):
                         print('i am in ow')
                         punchout= datetime.strptime(sorted_temp_df['PunchTime'].iloc[j],'%H:%M').time()
                         punchout_temp=str(sorted_temp_df['EffectiveDate'].iloc[j].date())+'T'+str(punchout)+'Z'
                         employee_time = {
                            "employeeId": empid ,
                            "ts_out": punchout_temp,
                            "use_localtime": True
                            }
                         print(employee_time)
                         if(k<len(timesheet_body)):
                           
                            timesheet_body[k]=employee_time
                            
                            timesheet_json=json.dumps(timesheet_body)
                            k=k+1
                         
                         j=j+1
                         
                    elif(sorted_temp_df['PunchType'].iloc[j]=='O' and sorted_temp_df['PunchType'].iloc[j+1]=='O'):
                        punchout= datetime.strptime(sorted_temp_df['PunchTime'].iloc[j],'%H:%M').time()
                        punchout_temp=str(sorted_temp_df['EffectiveDate'].iloc[j].date())+'T'+str(punchout)+'Z'
                        
                        punchout_sec= datetime.strptime(sorted_temp_df['PunchTime'].iloc[j+1],'%H:%M').time()
                        punchout_sec_temp=str(sorted_temp_df['EffectiveDate'].iloc[j+1].date())+'T'+str(punchout_sec)+'Z'
                        
                        employee_time = {
                            "employeeId": empid ,
                            "ts_out": punchout_temp,
                            "use_localtime": True
                            }
                        employee_sec_time={
                            "employeeId": empid ,
                            "ts_out": punchout_sec_temp,
                            "use_localtime": True
                            }
                        print(employee_time)
                        #print(employee_time_sec)
                        if(k+1<len(timesheet_body)):
                           
                            timesheet_body[k]=employee_time
                            timesheet_body[k+1]=employee_sec_time
                            timesheet_json=json.dumps(timesheet_body)
                            k=k+2
                        """
                        if(k_sec<len(timesheet_body_sec)):
                            timesheet_body_sec[k_sec]=employee_time_sec
                            timesheet_json_sec=json.dumps(timesheet_body_sec)
                            k_sec=k_sec+1
                        """
                        j=j+2
                        
                        
                        
                else:
                    if(sorted_temp_df['PunchType'].iloc[j]=='W'):
                        print('i am in w')
                        punchin= datetime.strptime(sorted_temp_df['PunchTime'].iloc[j],'%H:%M').time()
                        punchin_temp=str(sorted_temp_df['EffectiveDate'].iloc[j].date())+'T'+str(punchin)+'Z'
                        punchout_temp= str(sorted_temp_df['EffectiveDate'].iloc[j].date()+timedelta(1))+'T'+str(punchin)+'Z'
                        employee_time = {
                            "employeeId": empid ,
                            "ts_in": punchin_temp,
                            "ts_out": punchout_temp,
                            "use_localtime": True
                            }
                        print(employee_time)
                        j=j+1
                        
                    elif(sorted_temp_df['PunchType'].iloc[j]=='O'):
                         print('i am in o')
                         punchout= datetime.strptime(sorted_temp_df['PunchTime'].iloc[j],'%H:%M').time()
                         punchout_temp=str(sorted_temp_df['EffectiveDate'].iloc[j].date())+'T'+str(punchout)+'Z'
                         employee_time = {
                            "employeeId": empid ,
                            "ts_out": punchout_temp,
                            "use_localtime": True
                            }
                         print(employee_time)
                         j=j+1
                         
                    if(k<len(timesheet_body)):
                           
                        timesheet_body[k]=employee_time
                            
                        timesheet_json=json.dumps(timesheet_body)
                        k=k+1
                        
                        
                print("final value of k---------------->>>%s",k)        
                if(k==len(timesheet_body) and left_count>=50):   
                            
                    print("value of k-------%s",k) 
                    print("i am in timesheet_body--------")
                    print(timesheet_body)
                    print(type(timesheet_json))
                    post_url = 'https://api.kogniz.com/enrolled-management/people/inoutBatchMulti'
                    response = requests.post(post_url,headers=timesheet_header,data=timesheet_json)
                    if ('error' in str(response.content) or 'error' in response):
                        log(response,'warn')
                        log(response.content,'warn')
                        log("Error occured posting timesheet record for  employee id",'console')
                    else:
                        log('successfull','console')
                        log(response,'warn')
                        log(response.content,'warn')
                    k=0
                    timesheet_body= [None]*50
                
                          
                elif(left_count<=50):
                    print("left_count-------------->>>%s",left_count)
                    left_timesheet_body.append(employee_time)
                    timesheet_json=json.dumps(left_timesheet_body)
                    print(timesheet_json)
                    post_url = 'https://api.kogniz.com/enrolled-management/people/inoutBatchMulti'
                    response = requests.post(post_url,headers=timesheet_header,data=timesheet_json)
                    if ('error' in str(response.content) or 'error' in response):
                        log(response,'warn')
                        log(response.content,'warn')
                        log("Error occured posting timesheet record for  employee id ",'console')
                    else:
                        log('successfull','console')
                        log(response,'warn')
                        log(response.content,'warn')
                        
                        
            """
            if(k_sec<=len(timesheet_body_sec) and timesheet_body_sec!=[]):
                    print("value of ksec--------------------------%s",k_sec)
                    print("i am in timesheet_bodysec-------------------")
                    
                    print(timesheet_body_sec)
                    print(type(timesheet_json_sec))
                    post_url = 'https://api.kogniz.com/enrolled-management/people/inoutBatchMulti'
                    response = requests.post(post_url,headers=timesheet_header,data=timesheet_json_sec)
                    if ('error' in str(response.content) or 'error' in response):
                        log(response,'warn')
                        log(response.content,'warn')
                        log("Error occured posting timesheet record for  employee id ",'console')
                    else:
                        log('successfull','console')
                        log(response,'warn')
                        log(response.content,'warn')
                    k_sec=0
                    timesheet_body_sec= [None]*50
            """
            print("completed for this id")
        
                


def fetch_file():

    

    file_list=[]
    path_latest = "tractor/"
    new_timesheet=None
    new_timesheet_ssc=None
    if(args.run_date is not None):
        dt=datetime.strptime(args.run_date, '%Y%m%d')
    else:
        if(args.run_day=='today' or args.run_day is None):
            dt = date.today()
        elif(args.run_day=='yest'):
            dt = date.today() - timedelta(4)
        else:
            log('Unexpected  value passed in day option','console')
            exit(1)

    run_date=dt.strftime("%Y%m%d")
    
    
    
    log('fetching files for below dates','console')
    log(run_date,'console')
    
            
    files_sdk.folder.list_for("tractor/")
    for f in files_sdk.folder.list_for(path_latest, {
        "page": 1,
        "sort_by[modified_at_datetime]": "desc"
    }):
        print(f.display_name)        
        timesheet_today_file = re.search("KognizPunchData_Today_%s" %run_date, f.display_name)
        #timesheet_yest_file = re.search("TEST_Kogniz_Yest_%s" %run_date, f.display_name)
        
        print(timesheet_today_file)
        if(timesheet_today_file!=None ):
            dt_object = datetime.now()
            log("run_date  file found",'console' )
            log(f.display_name,'console')
            f.download_file(f.display_name)
            
            run_date_timesheet = os.path.join(os.getcwd(), '%s'%f.display_name )
            new_timesheet = os.path.join(os.getcwd(), 'Kogniz_Today_%s_%s%s%s.csv'%(run_date,dt_object.hour,dt_object.minute,dt_object.second))
            os.rename(run_date_timesheet,new_timesheet)
            today_source_path = path_latest + f.display_name
            today_dest_path = "tractor/completed/" + f.display_name
            file_list.append(new_timesheet)
            log(new_timesheet,'console')
            files_sdk.file_action.move(today_source_path,{
            
                "destination": today_dest_path
                })
            log("Download completed",'console')
        """    
        elif(timesheet_yest_file!=None):
            log(f.display_name,'console')
            f.download_file(f.display_name)
            
            run_date_timesheet_ssc = os.path.join(os.getcwd(), '%s'%f.display_name )
            new_timesheet_ssc = os.path.join(os.getcwd(), 'Kogniz_Yesterday_%s.csv'%run_date)
            os.rename(run_date_timesheet_ssc,new_timesheet_ssc)
            yest_source_path = path_latest + f.display_name
            yest_dest_path = "tractor/completed/" + f.display_name
            file_list.append(new_timesheet_ssc)
            files_sdk.file_action.move(yest_source_path,{
            
                "destination": yest_dest_path
                })
            log(new_timesheet_ssc,'console')
            """
            
    if(file_list==[]):
        log("no files found",'console')
        err='Error: Timesheet run failed for current hour.File is missing on files.com.Please check files.com or contact tractor timesheet administartor.
        Log location: /home/ubuntu/temp. Check log on aws machine'

    email_client.send_mail('ERROR:TIMESHEET FILE NOT FOUND FOR CURRENT HOUR.Upload failed',err)

        exit(1)
    else:
        print("full file list")
        print(file_list)
        return(file_list)
        log("Fetched timesheet  file from Files.com successfully",'console')
    
    """
            
    if(new_timesheet!=None and new_timesheet_ssc!=None):
        log("Fetched both timesheet  file from Files.com successfully",'console')
        file_list.append(new_timesheet)
        file_list.append(new_timesheet_ssc)
        return file_list
    elif(new_timesheet!=None):
        print(new_timesheet)
        log("Fetched  timesheet main file from Files.com successfully",'console')
        file_list.append(new_timesheet)
        print(file_list)
        return file_list
    elif(new_timesheet_ssc!=None):
        log("Fetched both timesheet SSC file from Files.com successfully",'console')
        file_list.append(new_timesheet_ssc)
        return file_list
    else:
        log("No file found for run_date.Please check",'console')
        exit(1)
    """
 
def cleanup_call():
    status=''
    dt_object=datetime.now()
    cleanup_url='https://apps.kogniz.com/event-data/proximity/logbook/cleanup-clock-in-out'
    status_url = 'https://apps.kogniz.com/event-data/proximity/logbook/cleanup-clock-in-out-status'
    run_date= str(date.today())+'T'+str(dt_object.hour)+':00:00Z'
    print(run_date)
    
    cleanup_body={
        "ts_cutoff":run_date,
        "inclusions": [
                    {
                        "n": "Location",
                        "v": "Stores"
                        },
                    {
                        "n": "Compensation Frequency",
                        "v": "H"
                        }
                    ]
                }
    
    cleanup_body_json=json.dumps(cleanup_body)
    print(cleanup_body)
    
    clean_header={'X-Kogniz-api-key':'%s' %api_key,
            'accept':'application/json','Content-Type':'application/json'}
    status_header = {'X-Kogniz-api-key':'%s' %api_key,
            'accept':'application/json','Content-Type':'application/json'}
    
    response = requests.post(cleanup_url,headers=clean_header,data=cleanup_body_json) 
    if ('error' in str(response.content) or 'error' in response):
        log(response,'warn')
        log(response.content,'warn')
        log("Error in cleanup process call",'console')
        err='Error: Timesheet cleanup call  failed for current hour.Please check latest run log.
        Log location: /home/ubuntu/temp. Check log on aws machine'
        email_client.send_mail('ERROR:TIMESHEET CLEANUP PROCESS FAILED FOR CURRENT HOUR.',err)
        exit(1)

    else:
    
        log(response,'warn')
        log(response.content,'warn')
    while(status!='false'):
        response=requests.get(status_url,headers=status_header)
        print(type(response.content))
        print(response.content)
        status = re.search('\"WorkInProgress\":(.*)\,\"EventProcessed\":',str(response.content)).group(1)
        print(status)
        time.sleep(15)
     
    

        
def log(msg,code):
    
    if (code=='console'):
       print(msg)

    if(code=='warn'):
        logging.warning(msg)
        
def usage( ):
    print ("Error: -file1 or -file2 does not exist or wrng path given.Please check.")
    sys.exit(1)


    
if __name__ == "__main__":  
   

    files=getoptions()
    print("--------------%s"%files)
    if files!=False:
        prepare_dataframe(files)
    if args.flag is None:
        cleanup_call()    
    
        
        

