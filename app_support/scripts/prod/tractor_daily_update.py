# -*- coding: utf-8 -*-
"""
Created on Thu Sep 24 19:00:47 2020

@author: waheguru
"""

import pandas as pd
import numpy as np
import sys
import os
import re
import time
from datetime import datetime, date, time, timedelta
import argparse
import json
import requests
from ftplib import FTP_TLS
import files_sdk
import logging
import mail_client

###Setting up global variables

file1 = ''
file2 = ''
location_lst=[]
location_final=[]
location_dict={}
output_file =os.path.join(os.getcwd(), 'output' + '.xlsx')

files_sdk.set_api_key("69f4b6d7e54e66bbcfa6e2627fc4564586a66c97c0012442a513898fcc37cf04")


def getoptions():
    
    global arguments,api_key,args,today_source_path,today_dest_path,term_source_path,term_dest_path
    ap = argparse.ArgumentParser()

    ap.add_argument("-k", "--api_key",dest ='key_file',required=True,help="api key file path")
    ap.add_argument("-f1", "--file_today",dest ='file1',help="file asof today")
    ap.add_argument("-f2", "--file_yest",dest ='file2',help="file asof yesterday")
    ap.add_argument("-f3", "--term_today",dest ='file3',help="file of terminated employees")
    ap.add_argument("-T", "--terminate_only",help="only employee termination will run")
    args, leftovers = ap.parse_known_args()
    
    arguments = args
    
    if (os.path.isfile(args.key_file) !=True):
        
        log(os.path.isfile(args.key_file),'console')
        usage()
        
    else:
        with open(args.key_file, 'r') as file:
            api_key= file.read().replace('\n', '')
        
        
    if (args.file1 is  None) or (args.file2 is  None) or (args.file3 is  None):
          log("Fetching files from Files.com",'console')
          today_file,yest_file,term_file=fetch_file()
          return today_file,yest_file,term_file
    else   : 
        if (os.path.isfile(args.file1) !=True or os.path.isfile(args.file2) !=True):
            log(os.path.isfile(args.file1),'console')
            log(os.path.isfile(args.file2),'console')
            
            usage()
            
        else:
            log("file1:{}".format(args.file1),'console')
            log("file2{}".format(args.file2),'console')
            return args.file1,args.file2,args.file3


def dataframe_compare(df1, df2, which=None):
    #Find rows which are different between two DataFrames.
    
    comparison_df = df1.merge(df2,
                              indicator=True,
                              how='outer')
    if which is None:
        diff_df = comparison_df[comparison_df['_merge'] != 'both']
    else:
        diff_df = comparison_df[comparison_df['_merge'] == which]
    return diff_df
    #diff_df.to_csv('data/diff.csv')
    
    
def prepare_dataframe(file1,file2):
    cols = [0,2,3]
    df1 = pd.read_csv(file1)
    df2 = pd.read_csv(file2)

    df1.to_excel('today.xlsx',index=False)
    df2.to_excel('yesterday.xlsx',index=False)
    
    
    df3 = pd.read_excel('today.xlsx')
    df4 = pd.read_excel('yesterday.xlsx')
    df3_sorted=df3.sort_values(by=['Empl ID'], ascending=True)
    df4_sorted=df4.sort_values(by=['Empl ID'], ascending=True)
    
    df3_sorted = df3_sorted.apply(lambda x: x.astype(str).str.lower())
    df4_sorted = df4_sorted.apply(lambda x: x.astype(str).str.lower())
    
    
    
    both_df=dataframe_compare(df3_sorted, df4_sorted,which='both')
    right_df = dataframe_compare(df3_sorted, df4_sorted,which='right_only')
    final_df=dataframe_compare(df3_sorted, df4_sorted,which='left_only')
    final_dataframe=final_df.drop(columns=['_merge'])
    final_dataframe.dropna(axis=1, how='all')
    
    print("final_dataframe")
    
    print("Saving output file:{}".format(output_file))
    writer = pd.ExcelWriter(output_file, engine='xlsxwriter')
    final_dataframe.to_excel(writer,sheet_name='sheet1')
    writer.save()

    
    return final_dataframe,right_df
    
    
    
def api_call(emp_df,existing_emp_df):
    
    new_emp_count=0
    updated_emp_count=0

    location_url= 'https://api.kogniz.com/device-management/locations'
    location_header={'X-Kogniz-api-key':'%s' %api_key,
                     'accept':'application/json'}
    location_response=requests.get(location_url,headers=location_header)
    location_json=location_response.json()
    #print (len(location_json))
    
    for i in range(len(location_json)):
        #print(location_json[i]['name'])
        location_lst.append(location_json[i]['name'].lower())
        location_final.append(location_lst[i].replace(" ", ""))
        location_dict[location_final[i]]=location_json[i]['id']
        
    enroll_url = 'https://api.kogniz.com/enrolled-management/people'
    enroll_header = {'X-Kogniz-api-key':'%s' %api_key,
                     'accept':'application/json','Content-Type':'application/json'}
    
    

    
    existing_emp_df1 = emp_df.assign(result=emp_df['Empl ID'].isin(existing_emp_df['Empl ID']))
    existing_emp_df1=existing_emp_df1.loc[existing_emp_df1['result']]
    new_emp_df = emp_df[~emp_df['Empl ID'].isin(existing_emp_df['Empl ID'])]
    existing_emp_df1=existing_emp_df1.drop(columns=['result'])
    
    log("New emplyees found",'console')
    log(new_emp_df,'console')
    
    log("Existing emplyees who will be updated",'console')
    log(existing_emp_df1,'console')
    
    log("Started Posting new empl0yees",'console')
    for i in range(len(new_emp_df)):
        
        if (new_emp_df['Location'].iloc[i]=='stores'or new_emp_df['Location'].iloc[i]=='Petsense'):
            new_emp_df['Location'].iloc[i]='store'
        
        location_id=str(new_emp_df['Location'].iloc[i])+'-'+str(new_emp_df['Store'].iloc[i])
        
        location_id=location_id.replace(" ", "")
        #print(location_id)
        #print(location_dict.keys())
        if (location_id in location_dict.keys()):
                print('matched %s' %i)
               
      
                enroll_dict={}
                enroll_data = {
        	           "firstName" : new_emp_df['First Name'].iloc[i],
        	           "lastName" : new_emp_df['Last Name'].iloc[i],
                       "employeeId" : new_emp_df['Empl ID'].iloc[i],
                       "defaultLocationId" : location_dict.get(location_id),
        	           "notes" : "new employee tagid registered"
        	           }
        else:
                print("Not matched, running without location for id:%s" %new_emp_df['Empl ID'].iloc[i])
                 
                enroll_dict={}
                enroll_data = {
        	           "firstName" : new_emp_df['First Name'].iloc[i],
        	           "lastName" : new_emp_df['Last Name'].iloc[i],
                       "employeeId" : new_emp_df['Empl ID'].iloc[i],
        	           "notes" : "new employee tagid registered"
        	           }
                
        enroll_data_json = json.dumps(enroll_data)
        print("enroll_data_json")
        log(enroll_data_json,'console')
        response = requests.post(enroll_url,headers=enroll_header,data= enroll_data_json)
        
        
        if ('error' in str(response.content) or 'error' in response):
            log(response,'warn')
            log(response.content,'warn')
            log("Error occured fetching enrolled id for employee %s"%new_emp_df['Empl ID'].iloc[i],'console')
            continue
        elif(response==[]):
            log("No enroll id  found for emp id %s"%new_emp_df['Empl ID'].iloc[i] ,'warn')
            continue
            
        else:
            
            
            enroll_dict = response.json()
            enrolled_id =enroll_dict['enrolledId']
            print (enrolled_id)
        
            attribute_url = "https://api.kogniz.com/enrolled-management/people/%s/attributes" % enrolled_id
        
            attr_data = {
                        "attributes" : [
                          
                          
                          {"n": "Store" ,"v": new_emp_df['Store'].iloc[i]},
                          {"n": "District" ,"v": new_emp_df['District'].iloc[i]},
                          {"n": "Position Title" ,"v": new_emp_df['Position Title'].iloc[i]},
                          {"n": "Full/Part Time" ,"v": new_emp_df['Full/Part Time'].iloc[i]},
                          {"n": "Regular/Temporary" ,"v": new_emp_df['Regular/Temporary'].iloc[i]},
                          {"n": "Hire Date" ,"v": new_emp_df['Hire Date'].iloc[i]},
                          {"n": "Location" ,"v": new_emp_df['Location'].iloc[i]},
                          {"n": "Compensation Frequency" ,"v": new_emp_df['Compensation Frequency'].iloc[i]},
                          {"n": "State - Location Info" ,"v": new_emp_df['State - Location Info'].iloc[i]},
                          {"n": "Work Location" ,"v": new_emp_df['Work Location'].iloc[i]},
                          {"n": "Region" ,"v": new_emp_df['Region'].iloc[i]}
                          ]}
            log(attribute_url,'console')
            attribute_json = json.dumps(attr_data)
            log(attribute_json,'console')
            
            attribute_header = {'X-Kogniz-api-key':'%s' %api_key,
                         'accept':'*/*','Content-Type':'application/json'}
            attribute_response = requests.post(attribute_url,headers=attribute_header,data= attribute_json)
            if ('error' in attribute_response):
                log(attribute_response,'warn')
                log("Error occured while adding new employee %s"%new_emp_df['Empl ID'].iloc[i],'console')
                continue
            else:
                new_emp_count=new_emp_count+1
                log("Total number of new employees added %s" % new_emp_count,'console')

    log("existing employee update started",'console') 
    log(existing_emp_df1,'console')   
    
    for j in range(len(existing_emp_df1)):
        
                
        if (existing_emp_df1['Location'].iloc[j]=='stores' or existing_emp_df1['Location'].iloc[j]=='Petsense'):
            existing_emp_df1['Location'].iloc[j]='store'
        
        location_id=existing_emp_df1['Location'].iloc[j]+'-'+existing_emp_df1['Store'].iloc[j]
        
        location_id=location_id.replace(" ", "")
        put_data_json= None
        if (location_id in location_dict.keys()):
            print('matched %s' %j)
            print(location_id)
            enroll_dict={}
            enroll_data = {
    	           "firstName" : existing_emp_df1['First Name'].iloc[j],
    	           "lastName" : existing_emp_df1['Last Name'].iloc[j],
                   "sourceEnrolledId" : existing_emp_df1['Empl ID'].iloc[j],
                   "defaultLocationId" : location_dict.get(location_id),
    	           "notes" : "existing emplyee attributes updated"
    	           }
            put_data = {"defaultLocationId" : location_dict.get(location_id)}
            put_data_json = json.dumps(put_data)
        else:
            
            print(location_id)
            print("Not matched, running without location for id:%s" %existing_emp_df1['Empl ID'].iloc[j])
            enroll_dict={}
            enroll_data = {
    	           "firstName" : existing_emp_df1['First Name'].iloc[j],
    	           "lastName" : existing_emp_df1['Last Name'].iloc[j],
                   "sourceEnrolledId" : existing_emp_df1['Empl ID'].iloc[j],
    	           "notes" : "existing emplyee attributes updated"
    	           }
        get_url = "https://api.kogniz.com/enrolled-management/people?employeeId=%s" % existing_emp_df1['Empl ID'].iloc[j]
        
        log("geturl for enrolled id of existing employee",'console')
        log(get_url,'console')
        
        enroll_data_json = json.dumps(enroll_data)
        log(enroll_data_json,'console')
        
        response = requests.get(get_url,headers=enroll_header,data= enroll_data_json)
        
        enroll_dict = response.json()
        print(enroll_dict)
        if ('error' in str(response.content) or 'error' in response):
                log(response,'warn')
                log(response.content,'warn')
                log("Error occured fetching enrolled id for employee %s"%existing_emp_df1['Empl ID'].iloc[j],'console')
                continue
        elif(enroll_dict==[]):
                log("No enroll id  found for emp id %s"%existing_emp_df1['Empl ID'].iloc[j] ,'warn')
                #log("No enroll id  found for emp id",'warn')
                continue
        
        else:
           
            enrolled_id =enroll_dict[0]['id']
            attributes_list=existing_emp_df1.columns.values.tolist()
            my_string = ",".join(attributes_list)
            
            delete_url = "https://api.kogniz.com/enrolled-management/people/%s/attributes?attributes=%s" % (enrolled_id,my_string)
            put_url = "https://api.kogniz.com/enrolled-management/people/%s" % enrolled_id
            log(delete_url,'console')
            delete_response=requests.delete(delete_url,headers=enroll_header)
            if (put_data_json):
                log(put_url,'console')
                put_response=requests.put(put_url,headers=enroll_header,data=put_data_json)
                if('error' in put_response or 'error' in str(put_response.content)):
                    log(put_response,'warn')
                    log('failed to post location for existing emp id %s'%existing_emp_df1['Empl ID'].iloc[j],'console')
            if ('error' in delete_response):
                log(delete_response,'warn')
                log("Error occured while deleting attributes for existing emp %s"%existing_emp_df1['Empl ID'].iloc[j],'console')
                continue
            else:
                
                log("attributes deleted for emp %s"%existing_emp_df1['Empl ID'].iloc[j],'console')
            print(enrolled_id)
            attribute_url = "https://api.kogniz.com/enrolled-management/people/%s/attributes" % enrolled_id
            attr_data = {
    	                  "attributes" : [
                              
                              
                              
                              {"n": "Store" ,"v": existing_emp_df1['Store'].iloc[j]},
                              {"n": "District" ,"v": existing_emp_df1['District'].iloc[j]},
                              {"n": "Position Title" ,"v": existing_emp_df1['Position Title'].iloc[j]},
                              {"n": "Full/Part Time" ,"v": existing_emp_df1['Full/Part Time'].iloc[j]},
                              {"n": "Regular/Temporary" ,"v": existing_emp_df1['Regular/Temporary'].iloc[j]},
                              {"n": "Hire Date" ,"v": existing_emp_df1['Hire Date'].iloc[j]},
                              {"n": "Location" ,"v": existing_emp_df1['Location'].iloc[j]},
                              {"n": "Compensation Frequency" ,"v": existing_emp_df1['Compensation Frequency'].iloc[j]},
                              {"n": "State - Location Info" ,"v": existing_emp_df1['State - Location Info'].iloc[j]},
                              {"n": "Work Location" ,"v": existing_emp_df1['Work Location'].iloc[j]},
                              {"n": "Region" ,"v": existing_emp_df1['Region'].iloc[j]}
                              ]
                          }
            
            log('readding attributes','console')              
            log(attribute_url,'console')
            attribute_json = json.dumps(attr_data)
            log(attribute_json,'console')
            
            
            attribute_header = {'X-Kogniz-api-key':'%s' %api_key,
                         'accept':'*/*','Content-Type':'application/json'}
            attribute_response = requests.post(attribute_url,headers=attribute_header,data= attribute_json)
            if ('error' in attribute_response):
                log(attribute_response,'warn')
                log("Error occured while readding attributes for existing emp %s"%existing_emp_df1['Empl ID'].iloc[i],'console')
                continue
            else:
                updated_emp_count=updated_emp_count+1
            log("attributes readded for emp %s"%existing_emp_df1['Empl ID'].iloc[j],'console')
            log("Total number of existing employees updated %s" % updated_emp_count,'console')
            
           
        

def update_term_employee(term_file):
    
 
    log("updating terminated employees now",'console')
    df1 = pd.read_csv(term_file)
    df1.to_excel('today.xlsx',index=False)
    
    df2 = pd.read_excel('today.xlsx')
    df2_sorted=df2.sort_values(by=['Employee ID'], ascending=True)
    
    header = {'X-Kogniz-api-key':'%s' %api_key,
                     'accept':'application/json','Content-Type':'application/json'}
    get_tag_url = "https://api.kogniz.com/enrolled-management/labels"
    
    response = requests.get(get_tag_url,headers=header)
    tag_response = response.json()
    if ('error' in tag_response):
        log(tag_response,'warn')
        log("Tag Id Error occured while fetching tag codes.Cannot proceed without tagid for TERM tag",'console')
        exit(1)
    else:
        
        log(tag_response,'console')
        term_emp_count=0
        for key in tag_response:
            if(key['name']=='TERM' or key['name']=='term' or key['name']=='Term'):
            
                label_id=key['id']
        
        print(label_id)
        for i in range(len(df2_sorted)):
            
            put_tag_url = "https://api.kogniz.com/enrolled-management/people/source/%s" %df2_sorted['Employee ID'].iloc[i]
            put_data = {
    	           
                   "tagId": "[%s]" %-df2_sorted['Employee ID'].iloc[i],
                   "labelIds": [label_id],
    	           "notes" : "Employee terminated"
    	           }
            print (put_data)
            enroll_data_json = json.dumps(put_data)
            
            response = requests.put(put_tag_url,headers=header,data=enroll_data_json )
            tag_update_response = response.json()
            if ('error' in tag_update_response):
                log("Error found while updating label and tag_id for terminated employee with id :%s" %df2_sorted['Employee ID'].iloc[i],'console')
                log(tag_update_response,'warn')
                continue
            else:
                print("Label for employees id %s successfully updated" %df2_sorted['Employee ID'].iloc[i])
                term_emp_count=term_emp_count+1
        log('Toatl number of employee terminated%s'%term_emp_count,'console')
        
        files_sdk.file_action.move(term_source_path,{
            
                "destination": term_dest_path
                })
        files_sdk.file_action.move(today_source_path,{
            
                "destination": today_dest_path
                })
        
        
def fetch_file():

    files_sdk.folder.list_for("tractor/completed/")

    path_completed = "tractor/completed/"
    path_latest = "tractor/"
    dt = date.today()
    dt_yest=dt - timedelta(1)
    #dt = dt - timedelta(1)
    
 
    
    
    
    today=dt.strftime("%m%d%y")
    yesterday=dt_yest.strftime("%m%d%y")
    
    
    log('fetching files for below dates','console')
    log(today,'console')
    log(yesterday,'console')

    for f in files_sdk.folder.list_for(path_completed, {
        "page": 1,
        "sort_by[modified_at_datetime]": "desc"
    }):
        
        yest_active = re.search("Kogniz-TSCActive.%s" %yesterday, f.display_name)
        if (yest_active!=None):
            log ("yest active file found",'console')
            log(f.display_name,'console')
            f.download_file(f.display_name)
            yest_active_file = os.path.join(os.getcwd(), '%s'%f.display_name )
            log(yest_active_file,'console')

        else:
          log("File not required",'console')
          log(f.display_name,'console')  
    
    files_sdk.folder.list_for("tractor/")
    for f in files_sdk.folder.list_for(path_latest, {
        "page": 1,
        "sort_by[modified_at_datetime]": "desc"
    }):
    
        
        today_term = re.search("KognizTerminationFile.%s" %today, f.display_name)
        today_active = re.search("Kogniz-TSCActive.%s" %today, f.display_name)
        
        
        if(today_term!=None):
            log("today term file found",'console' )
            log(f.display_name,'console')
            f.download_file(f.display_name)
            today_term_file = os.path.join(os.getcwd(), '%s'%f.display_name )
            log(today_term_file,'console')
            term_source_path = path_latest + f.display_name
            term_dest_path = "tractor/completed/" + f.display_name
            
            files_sdk.file_action.move(term_source_path,{
            
                "destination": term_dest_path
                })
            
        elif(today_active!=None):
            log ("today active file found",'console')
            log(f.display_name,'console')
            f.download_file(f.display_name)
            today_active_file = os.path.join(os.getcwd(), '%s'%f.display_name )
            log(today_active_file,'console')
            today_source_path = path_latest + f.display_name
            today_dest_path = "tractor/completed/" + f.display_name
            
            files_sdk.file_action.move(today_source_path,{
            
                "destination": today_dest_path
                })
            

            
        else:
          log("nothing found",'console')
          log(f.display_name,'console')
        
    try:
        today_active_file and yest_active_file and today_term_file
    except NameError:
        err='Error: Any one or all the required files from Files.com is missing. Please check current working directory for missing file or contact tractor administrator'
        log(err,'warn')
        mail_client.send_mail('ERROR:Tractor daily employee file not found for today',err)
        sys.exit(1)
    else:
        log("Fetched files from Files.com successfully",'conole')
        return today_active_file , yest_active_file,today_term_file
        
    print("file downloaded")

def send_mail():
   
    
    msg = EmailMessage()
    msg['Subject'] = 'The contents of {textfile}'
    msg['From'] = 'prabhjot@kogniz.com'
    msg['To'] = 'prabhjot@kogniz.com'     
    s = smtplib.SMTP('localhost:25')
    s.send_message(msg)
    s.quit()
    print ("Successfully sent email")
    
    
    
def log(msg,code):
    
    if (code=='console'):
       print(msg)

    if(code=='warn'):
        logging.warning(msg)
"""       
def check_api_response(response,msg,emp_id):
    if ('error' in response):
        log(response,'warn')
        log("Error occured while readding attributes for existing emp %s"%emp_id,'console')
        continue
"""    
    
    
def usage( ):
    print ("Error: -file1 or -file2 does not exist or wrng path given.Please check.")
    sys.exit(1)
    
if __name__ == "__main__":  
    
    #send_mail()
    
    today_file,yest_file,term_file=getoptions()
    if args.terminate_only is not None:
        print("only termination process will run")
        update_term_employee(term_file)
    else:
        print("whole process will run")
        final_dataframe, right_df = prepare_dataframe(today_file,yest_file)
        api_call(final_dataframe,right_df)
        update_term_employee(term_file)
    
    
    



