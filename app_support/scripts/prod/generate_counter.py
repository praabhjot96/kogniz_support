#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon May  3 00:02:03 2021

@author: prabhjotsingh
"""

import pandas as pd
import numpy as np
import sys
import os
import re
import time
from datetime import datetime, date, time, timedelta
import argparse
import json
import requests
import logging
import time
from datetime import timezone
import mail_client2

def get_counterid() :
    
    
    api_key="b2c41470f1c8908854538d4c93efa5e2316962bdc21d3fa3"
    enroll_header = {'X-Kogniz-api-key':'%s' %api_key,
                     'accept':'application/json','Content-Type':'application/json'}
    get_url = "https://api.kogniz.com/count-data/counters"
    
    
    response = requests.get(get_url,headers=enroll_header)
    
    enroll_dict = response.json()
    print(datetime.utcnow() )
    utc_end = datetime.utcnow() + timedelta(minutes=1)
    utc_start= utc_end-timedelta(hours=1)
    #rounded = utc_now - timedelta(minutes=utc_now.minute % 1 + 1,
     #                     seconds=utc_now.second,
      #                    microseconds=utc_now.microsecond)
   
    starttime=str(utc_start.strftime("%Y-%m-%d"))+'T'+str(utc_start.strftime("%H:%M:%S"))+'.000Z'
    endtime=str(utc_end.strftime("%Y-%m-%d"))+'T'+str(utc_end.strftime("%H:%M:%S"))+'.000Z'
    print(starttime)
    print(endtime)
    j=0
    counter_dict={}
    for i in range(len(enroll_dict)):

        print(enroll_dict[i]['id'])
        get_counter_url="https://api.kogniz.com/count-data/counters/%s?startTime=%s&endTime=%s&frequency=hour" %(enroll_dict[i]['id'],starttime,endtime)

        print(get_counter_url)
        response = requests.get(get_counter_url,headers=enroll_header)

        if(response==[]or response.json()==[]):
            #mail_client2.send_mail("COUNTER EMAIL",'Counter value not found')
            print('No counter found for this id %s.Exiting.....'%enroll_dict[i]['id'])
            j=j+1
            #exit(0)
        else:

            print(response.json())
            counter_response=response.json()
            print(counter_response[0]['count'])
            counter_dict[enroll_dict[i]['name']]=counter_response[0]['count']
    if(j==3):
        mail_client2.send_mail("COUNTER EMAIL",'Counter value not found')
        print('email_sent for empty counter')
    else:

        print(counter_dict)
        msg_body='TIME:'+starttime+'\n'+str(counter_dict)
        print(msg_body)
        mail_client2.send_mail("COUNTER EMAIL",msg_body)
        print('email_sent')

if __name__ == "__main__":  
    
    get_counterid()
    
    
